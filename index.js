var list = require("./list.js")
var db = require("./db");
var http_port = process.env.PORT || 5000;

Load();
printWelcomeMessage();
interactiveConsole();

function input(question, callback) {
  var stdin = process.stdin,
    stdout = process.stdout;
  stdin.resume();
  stdout.write(question);
  stdin.once('data', function(data) {
    data = data.toString().trim();
    callback(data);
  });
}

function interactiveConsole() {
  input(">> ", function(data) {

    var parts = data.trim().split(' ');
    if (parts[0]) parts[0] = parts[0].toUpperCase();

    switch (true) {
      case (parts[0] == "Q"):
        process.exit(0);
        break;
      case (parts[0] == "LIST"):

        List();
        break;
      case (parts[0] == "ADD"):
        list.Add(parts[1], parts[2], parts[3]);
        break;
      case (parts[0] == "DEL"):
        list.Del(parts[1]);
        break;
      case (parts[0] == "SAVEDB"):
        Save();
        break;
      case (parts[0] == "LOADDB"):
        Load();
        break;
      default:
        console.log("Incorrect command");
        break;
    }
    interactiveConsole();
  });
}

function printWelcomeMessage() {
  console.log(['Avaialble commands:',
    'Q - Exits the program',
    'LIST - Lists all the characters',
    'ADD - Adds a character (NAME, POWER, TYPE)',
    'DEL - Deletes a character (NAME)',
    'SAVEDB - Saves the list',
    'LOADDB - Loads a list'
  ].join("\n"));
}

function List() {
  var characterList = list.List();
  for (var i = 0; i < characterList.length; i++) {
    console.log('\n' + 'Name: ' + characterList[i].name + '\n' + 'Power: ' + characterList[i].power + '\n' + 'Type: ' + characterList[i].type);
  }
}

function Save() {
  db.query("DELETE FROM characters", null, function(){
    var characterList = list.List();
    var queryArray = [];
    console.log("Characters saved: " + characterList.length);
    for (var i = 0; i < characterList.length; i++) {
      queryArray.push("INSERT INTO characters VALUES ('" + characterList[i].name + "', " + parseInt(characterList[i].power) + ",'" + characterList[i].type + "')");
    }
    db.query(queryArray.join(";"), null, function(err, res) {
      if (err) return console.log("Error while saving to DB");
      console.log('Saved!');
    });
  });
}

function Load() {
  list.Clear();

  db.query("SELECT * FROM characters", null, function(err, res) {
    if (err) throw err;
    var characterProps = res.rows;
    console.log("Characters loaded: " + characterProps.length); //Comprobar si el personatge carregat és vàlid
    for (var i = 0; i < characterProps.length; i++) {
      list.Add(characterProps[i].name, characterProps[i].power, characterProps[i].type);
    }
    console.log('Loaded!');
  });
}
