function Character(n, p, t) {
  return {
    name : n,
    power : p,
    type : t
  };
}

var characters = [];

exports.List = function() {
  return characters;
}

exports.Add = function(name, power, type) {
  var exists = characters.find(function(element){
    return element.name === name;
  })
  characters.push(Character(name, power, type));
}

exports.Del = function(name) {
  for (var i = 0; i < characters.length; i++){
      if (characters[i].name == name)
      {
          characters.splice(i, 1);
          console.log("Deleted: " + name);
          return;
      }
  }
  console.log("Could not find " + name);
}

exports.Clear = function(){
  characters = [];
}
